var express = require('express');
var router = express.Router();


//GET all playlists
router.get('/', function(req, res){
    req.getConnection(function(err, connection){
        connection.query("SELECT * FROM playlist", function(err, results){
            if(err){
                console.log(err);
            }
            res.json(results);
        });
    });
});

//GET playlist by id
router.get('/:playlist_id(\\d+)', function(req, res){
    req.getConnection(function(err, connection){
        connection.query("SELECT * FROM playlist WHERE id = ?", [req.params.playlist_id], function(err, results){
            if(err){
                console.log(err);
            }
            res.json(results);
        });
    });
});

//update playlist by id
router.put('/:playlist_id', function(req, res){
    res.json({
        id: 3,
        name: "playlist 3 has been updated"
    })
});

//delete playlist by id
router.delete('/:playlist_id', function(req, res){
    res.json({
        id: 4,
        name: "playlist 4 has been deleted"
    })
});

//create playlist
router.post('/', function(req, res){
    res.json({
        id: 5,
        name: "playlist 5 has been created"
    })
});

module.exports = router;