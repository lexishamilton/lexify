var express = require('express');
var router = express.Router();


//GET all record companies
router.get('/', function(req, res) {
   req.getConnection(function(err, connection){
        connection.query("SELECT * FROM record_company", function(err, results){
            if(err){
                console.log(err);
            }
            res.json(results);
        });
    }); 
});

//GET record company by id
router.get('/:record_company_id(\\d+)', function(req, res) {
   req.getConnection(function(err, connection){
        connection.query("SELECT * FROM record_company WHERE id = ?", [req.params.record_company_id], function(err, results){
            if(err){
                console.log(err);
            }
            res.json(results);
        });
    }); 
});

//update record company by id
router.put('/:record_company_id', function(req, res) {
   res.json({
    id:req.params.song_id,
    name: "Record Company 2 was updated"
   }); 
});

//delete record company by id
router.delete('/:record_company_id', function(req, res) {
   res.json({
    id:req.params.song_id,
    name: "Record Company 3 was deleted"
   }); 
});

//create record company
router.post('/', function(req, res) {
   res.json({
    id:4,
    name: "Record Company 4 was created"
   }); 
});

module.exports = router;