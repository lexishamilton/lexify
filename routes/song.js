var express = require('express');
var router = express.Router();


//GET all songs
router.get('/', function(req, res) {
   req.getConnection(function(err, connection){
        connection.query("SELECT * FROM song", function(err, results){
            if(err){
                console.log(err);
            }
            res.json(results);
        });
    }); 
});

//GET song by id
router.get('/:song_id(\\d+)', function(req, res) {
   req.getConnection(function(err, connection){
        connection.query("SELECT * FROM song WHERE id = ?", [req.params.song_id], function(err, results){
            if(err){
                console.log(err);
            }
            res.json(results);
        });
    });  
});

//update song by id
router.put('/:song_id', function(req, res) {
   res.json({
    id:req.params.song_id,
    name: "Song 2 was updated"
   }); 
});

//delete song by id
router.delete('/:song_id', function(req, res) {
   res.json({
    id:req.params.song_id,
    name: "Song 3 was deleted"
   }); 
});

//create song 
router.post('/', function(req, res) {
   res.json({
    id:4,
    name: "Song 4 was created"
   }); 
});

module.exports = router;