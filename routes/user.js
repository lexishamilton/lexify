var express = require('express');
var router = express.Router();


//GET all users
router.get('/', function(req, res) {
       req.getConnection(function(err, connection){
        connection.query("SELECT * FROM user", function(err, results){
            if(err){
                console.log(err);
            }
            res.json(results);
        });
    });
});

//GET user by id
router.get('/:user_id(\\d+)', function(req, res) {
   req.getConnection(function(err, connection){
        connection.query("SELECT * FROM user WHERE id = ?", [req.params.user_id], function(err, results){
            if(err){
                console.log(err);
            }
            res.json(results);
        });
    }); 
});

//update user by id
router.put('/:user_id', function(req, res) {
   res.json({
    id:req.params.song_id,
    name: "user 2 was updated"
   }); 
});

//delete user by id
router.delete('/:user_id', function(req, res) {
   res.json({
    id:req.params.song_id,
    name: "user 3 was deleted"
   }); 
});

//create user 
router.post('/', function(req, res) {
   res.json({
    id:4,
    name: "user 4 was created"
   }); 
});

module.exports = router;