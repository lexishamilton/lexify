var express = require('express');
var router = express.Router();


//GET all albums
router.get('/', function(req, res) {
    req.getConnection(function(err, connection){
        connection.query("SELECT * FROM album", function(err, results){
            if(err){
                console.log(err);
            }
            res.json(results);
        });
    });
});

//GET album by id
router.get('/:album_id(\\d+)', function(req, res) {
   req.getConnection(function(err, connection){
        connection.query("SELECT * FROM album WHERE id = ?", [req.params.album_id], function(err, results){
            if(err){
                console.log(err);
            }
            res.json(results);
        });
    }); 
});

//update album by id
router.put('/:album_id', function(req, res) {
   res.json({
    id:req.params.album_id,
    name: "Pete the Cat was updated"
   }); 
});

//delete album by id
router.delete('/:album_id', function(req, res) {
   res.json({
    id:req.params.album_id,
    name: "Pete the Cat was deleted"
   }); 
});

//create album 
router.post('/', function(req, res) {
   res.json({
    id:4,
    name: "Pete the Cat was created"
   }); 
});

module.exports = router;