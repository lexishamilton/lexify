var express = require('express');
var router = express.Router();


//GET all artists
router.get('/', function(req, res){
    req.getConnection(function(err, connection){
        connection.query("SELECT * FROM artist", function(err, results){
            if(err){
                console.log(err);
            }
            res.json(results);
        });
    });
});

//GET artist by id
router.get('/:artist_id(\\d+)', function(req, res){
    req.getConnection(function(err, connection){
        connection.query("SELECT * FROM artist WHERE id = ?", [req.params.artist_id], function(err, results){
            if(err){
                console.log(err);
            }
            res.json(results);
        });
    });
});

//update artist by id
router.put('/:artist_id', function(req, res){
    res.json({
        id: 3,
        name: "artist 3 has been updated"
    })
});

//delete artist by id
router.delete('/:artist_id', function(req, res){
    res.json({
        id: 4,
        name: "artist 4 has been deleted"
    })
});

//create artist
router.post('/', function(req, res){
    res.json({
        id: 5,
        name: "artist 5 has been created"
    })
});

module.exports = router;